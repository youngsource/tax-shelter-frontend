import VueI18n from "vue-i18n";
type LocaleMessages = VueI18n.LocaleMessages;

export class I18nRepository {
    private localStorage: Storage;
    private readonly defaultLanguage: string;
    private readonly translations: LocaleMessages;

    constructor(localStorage: Storage, defaultLanguage: string, translations: LocaleMessages) {
        this.localStorage = localStorage;
        this.defaultLanguage = defaultLanguage;
        this.translations = translations;
    }

    public language(): string {
        const lang = this.localStorage.getItem('lang');
        if (lang === null) {
            this.localStorage.setItem('lang', this.defaultLanguage);
            return this.defaultLanguage;
        }

        return lang;
    }

    public getTranslations(): LocaleMessages {
        return this.translations;
    }
}