export default class TaxShelterDatabag {
    aanslagjaar;
    maxTaxShelter;
    taxShelterInvestering;
    maxLiqReserve;
    liqReserve;
    isVerlaagdTarief;
    isVermeerdering;
    winstVoorBelastingen;
    dividenden;
    tantiemes;
    andereMutaties;
    andereVerworpenUitgavenGeenAftrek;
    andereVerworpenUitgavenWelAftrek;
    aftrekBewerking;
    voorafBetalingKwartaal1;
    voorafBetalingKwartaal2;
    voorafBetalingKwartaal3;
    voorafBetalingKwartaal4;
    terugBetaalbareVoorheffing;
    regularisatiesVorigeJaren;
    aftrekkenMetKorfbeperking;
    paymentDate;

    static copy(input) {
        let tbr = new TaxShelterDatabag;
        tbr.aanslagjaar = input.aanslagjaar;
        tbr.maxTaxShelter = input.maxTaxShelter;
        tbr.taxShelterInvestering = input.taxShelterInvestering;
        tbr.maxLiqReserve = input.maxLiqReserve;
        tbr.liqReserve = input.liqReserve;
        tbr.isVerlaagdTarief = input.isVerlaagdTarief;
        tbr.isVermeerdering = input.isVermeerdering;
        tbr.winstVoorBelastingen = input.winstVoorBelastingen;
        tbr.dividenden = input.dividenden;
        tbr.tantiemes = input.tantiemes;
        tbr.andereMutaties = input.andereMutaties;
        tbr.andereVerworpenUitgavenGeenAftrek = input.andereVerworpenUitgavenGeenAftrek;
        tbr.andereVerworpenUitgavenWelAftrek = input.andereVerworpenUitgavenWelAftrek;
        tbr.aftrekBewerking = input.aftrekBewerking;
        tbr.voorafBetalingKwartaal1 = input.voorafBetalingKwartaal1;
        tbr.voorafBetalingKwartaal2 = input.voorafBetalingKwartaal2;
        tbr.voorafBetalingKwartaal3 = input.voorafBetalingKwartaal3;
        tbr.voorafBetalingKwartaal4 = input.voorafBetalingKwartaal4;
        tbr.terugBetaalbareVoorheffing = input.terugBetaalbareVoorheffing;
        tbr.regularisatiesVorigeJaren = input.regularisatiesVorigeJaren;
        tbr.aftrekkenMetKorfbeperking = input.aftrekkenMetKorfbeperking;
        tbr.paymentDate = input.paymentDate;
        return tbr;
    }

    equals(input) {
        return this.aanslagjaar === input.aanslagjaar &&
            this.maxTaxShelter === input.maxTaxShelter &&
            this.taxShelterInvestering === input.taxShelterInvestering &&
            this.maxLiqReserve === input.maxLiqReserve &&
            this.liqReserve === input.liqReserve &&
            this.isVerlaagdTarief === input.isVerlaagdTarief &&
            this.isVermeerdering === input.isVermeerdering &&
            this.winstVoorBelastingen === input.winstVoorBelastingen &&
            this.dividenden === input.dividenden &&
            this.tantiemes === input.tantiemes &&
            this.andereMutaties === input.andereMutaties &&
            this.andereVerworpenUitgavenGeenAftrek === input.andereVerworpenUitgavenGeenAftrek &&
            this.andereVerworpenUitgavenWelAftrek === input.andereVerworpenUitgavenWelAftrek &&
            this.aftrekBewerking === input.aftrekBewerking &&
            this.voorafBetalingKwartaal1 === input.voorafBetalingKwartaal1 &&
            this.voorafBetalingKwartaal2 === input.voorafBetalingKwartaal2 &&
            this.voorafBetalingKwartaal3 === input.voorafBetalingKwartaal3 &&
            this.voorafBetalingKwartaal4 === input.voorafBetalingKwartaal4 &&
            this.terugBetaalbareVoorheffing === input.terugBetaalbareVoorheffing &&
            this.regularisatiesVorigeJaren === input.regularisatiesVorigeJaren &&
            this.aftrekkenMetKorfbeperking === input.aftrekkenMetKorfbeperking &&
            this.paymentDate === input.paymentDate;
    }
}
