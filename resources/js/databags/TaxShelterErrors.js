export default class TaxShelterErrors {
    aanslagjaar;
    maxTaxShelter;
    taxShelterInvestering;
    maxLiqReserve;
    liqReserve;
    isVerlaagdTarief;
    isVermeerdering;
    winstVoorBelastingen;
    dividenden;
    tantiemes;
    andereMutaties;
    andereVerworpenUitgavenGeenAftrek;
    andereVerworpenUitgavenWelAftrek;
    aftrekBewerking;
    voorafBetalingKwartaal1;
    voorafBetalingKwartaal2;
    voorafBetalingKwartaal3;
    voorafBetalingKwartaal4;
    terugBetaalbareVoorheffing;
    regularisatiesVorigeJaren;
    aftrekkenMetKorfbeperking;
}

