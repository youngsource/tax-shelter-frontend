import IteratieResult from "./IteratieResult.js";

export default class TaxShelterResponseDatabag {
    tussentotaalGrondslagVenB = new IteratieResult;
    korfBeperkingen = new IteratieResult;
    grondslagVenb = new IteratieResult;
    belastingenOpDeGrondslag = new IteratieResult;
    grondslagVermeerdering = new IteratieResult;
    berekeningVanDeVermeerdering = new IteratieResult;
    geraamdeBelastingenGewoon = new IteratieResult;
    totaleBelastingenGewoon = new IteratieResult;
    grondslagLiquidatieReserve = new IteratieResult;
    belastingenLiquidatieReserve = new IteratieResult;
    verworpenUitgavenBelastingen = new IteratieResult;
    aanpassingenInMeer = new IteratieResult;
    maximaleVrijstelling = new IteratieResult;
    maximaleInvestering = new IteratieResult;
    iteraties = [ {
        belasteReserve : "",
        taxShelterKost : "",
        belastingsvrijeReserve : "",
        taxShelterVrijstelling : "",
    }];
}
