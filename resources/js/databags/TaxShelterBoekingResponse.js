import TaxShelterRamingResponse from "./TaxShelterRamingResponse.js";

export default class TaxShelterBoekingResponse {
    raming = new TaxShelterRamingResponse;
    investeringTaxShelterMogelijk;
    positieveGeraamdeBelastingen;
    d499010;
    c489010;
    d689010;
    c132010;
    d670210;
    d495000;
    c499010;
    d670200;
    c450000;
    d412000;
    c670100;
    d670220;
    c450010;
    d692100;
    c133030;
    d489010;
    c550010;
    d550010;
    c751010;
    d132010;
    c789010;
    d689010Next;
    c132010Next;
}
