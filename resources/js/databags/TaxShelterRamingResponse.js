import TaxShelterResponseColumnDatabag from "./TaxShelterResponseColumnDatabag.js";
import TaxShelterResponseDatabag from "./TaxShelterResponseDatabag.js";

export default class TaxShelterRamingResponse {
    taxShelterOutput = new TaxShelterResponseDatabag;
    winstVoorBelastingen = new TaxShelterResponseColumnDatabag;
    dividenden = new TaxShelterResponseColumnDatabag;
    tantiemes = new TaxShelterResponseColumnDatabag;
    belastingvrijeReserveTaxShelter = new TaxShelterResponseColumnDatabag;
    vrijstellingTaxShelter = new TaxShelterResponseColumnDatabag;
    belasteReserveTaxShelter = new TaxShelterResponseColumnDatabag;
    andereMutatiesReserves = new TaxShelterResponseColumnDatabag;
    andereVerworpenUitgavenGeenAftrekverbod = new TaxShelterResponseColumnDatabag;
    andereVerworpenUitgavenWelAftrekverbod = new TaxShelterResponseColumnDatabag;
    aftrekBewerkingen = new TaxShelterResponseColumnDatabag;
    grondslagVenB = new TaxShelterResponseColumnDatabag;
    belastingenOpDeGrondslag = new TaxShelterResponseColumnDatabag;
    berekeningVanDeVermeerdering = new TaxShelterResponseColumnDatabag;
    voorafbetalingQ1 = new TaxShelterResponseColumnDatabag;
    voorafbetalingQ2 = new TaxShelterResponseColumnDatabag;
    voorafbetalingQ3 = new TaxShelterResponseColumnDatabag;
    voorafbetalingQ4 = new TaxShelterResponseColumnDatabag;
    terugbetaalbareVoorheffingGewoon = new TaxShelterResponseColumnDatabag;
    geraamdeBelastingenGewoon = new TaxShelterResponseColumnDatabag;
    regularisatieVorigeJaren = new TaxShelterResponseColumnDatabag;
    belastingLiquidatieReserve = new TaxShelterResponseColumnDatabag;
    aanpassingInMeer = new TaxShelterResponseColumnDatabag;
    nietAftrekbareTaxShelterKost = new TaxShelterResponseColumnDatabag;
    samenvatting = new TaxShelterResponseColumnDatabag;
    aggregatie = {
        maximaleTaxShelterInvestering: '',
        maximaleTaxShelterVrijstelling: '',
        belastingZonderTaxShelterInvestering: '',
        belastingMetTaxShelterInvestering: '',
        effectieveTaxShelterInvestering: '',
        resterendeInvesteringsRuimte: '',
        overinvestering: '',
        isOverinvestering: false,
    };
    ratios = {
        fiscaalRendement: '',
        financieelRendement: '',
        nettoFinancieelRendement: '',
        totaalRendement: '',
        fiscaalRendementPercentage: '',
        financieelRendementPercentage: '',
        nettoFinancieelRendementPercentage: '',
        totaalRendementPercentage: '',
        euribor: '',
        gewoonTarief: '',
        indexDatum: '',
        overdrachtRendement: '',
        overdrachtRendementsPercentage: ''
    };
    aangelegdeLiquidatieReserve;
}
