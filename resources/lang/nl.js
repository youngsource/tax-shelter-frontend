export default {
    tax_shelter: {
        title: 'Tax shelter tool',
        info: `<p><b>toelichting</b></p>
            <p>Welkom bij de tax shelter simulator. Deze simulator is er om u te helpen bij:</p>
            <ul>
                <li> Uw maximale tax shelter investering te berekenen</li>
                <li> Het rendement van uw investering te bepalen</li>
                <li> U te helpen bij het correct boeken van de tax shelter investering</li>
            </ul>
            <p>
                Het maximaal te investeren bedrag en het rendement wordt bepaald op basis van verschillende
                iteraties gebaseerd op de ingegeven boekhoudkundige en fiscale situatie van de investeerder.
            </p>
            <p>
                U heeft de keuze om de simulatie te maken met een door u opgegeven tax shelter investering of de
                tool de maximale tax shelter investering zelf te laten berekenen, en dit bij een maximale aanleg van
                de liquidatiereserve of met een zelf opgegeven bedrag.
            </p>
            <aside>
                Let op! Uw tax shelter rendement hangt af van heel veel factoren. Zo kan indien het verlaagd tarief
                in de vennootschapsbelasting van toepassing is kan het rendement van de maximale tax
                shelterinvestering
                negatief zijn.
            </aside>
            <p>
                De tool geeft u een duidelijke eerste indicatie maar het is aangewezen steeds een accountant of
                belastingconsulent te raadplegen.
            </p>`,
        general: 'Algemeen',
        fiscal_year: 'Aanslagjaar',
        max_tax_shelter: 'Maximale tax shelter investering',
        yes: 'Ja',
        no: 'Nee',
        optimal_tax_shelter: 'Optimale tax shelter',
        tax_shelter_investment: 'Tax shelter investering',
        max_liquidation_reserve: 'Maximale liquidatiereserve',
        liquidation_reserve: 'Liquidatiereserve',
        increment_va: 'Vermeerdering VA',
        decreased_tariff: 'Verlaagd tarief',
        accountancy: 'Boekhoudkundig',
        profit_before_taxes: 'Winst voor belastingen',
        dividends: 'Dividenden',
        tantiemes: 'Tantiemes',
        other_fiscal_corrections: 'Andere fiscale correcties',
        other_disposed_expenses_no_deduction: 'Andere verworpen uitgaven (geen aftrek)',
        other_disposed_expenses_with_deduction: 'Andere verworpen uitgaven (wel aftrek)',
        deduction_calculations: 'Aftrekbewerkingen',
        deduction_calculations_without_grain_limitation: 'aftrekbewerkingen zonder korfbeperking',
        deduction_calculations_with_grain_limitation: 'aftrekbewerkingen met korfbeperking',
        prepaid_quarter_1: 'Voorafbetaling 1<sup>e</sup> Kwartaal',
        prepaid_quarter_2: 'Voorafbetaling 2<sup>e</sup> Kwartaal',
        prepaid_quarter_3: 'Voorafbetaling 3<sup>e</sup> Kwartaal',
        prepaid_quarter_4: 'Voorafbetaling 4<sup>e</sup> Kwartaal',
        reimbursable_withholding_taxes: 'Terugbetaalbare voorheffingen',
        adjustments_previous_years: 'Regularisaties vorige jaren',
        calculate: 'Bereken',
        calculation_not_representative: 'Opgelet! De cijfers zijn verschillend van die gebruikt werden in de uitkomst. Klik opnieuw op de bereken knop om het resultaat te vernieuwen.',
        hide_bookings: 'Boekingen Verbergen',
        show_bookings: 'Boekingen Tonen',
        signature_framework_agreement: 'Ondertekening van de raamovereenkomst',
        payment_framework_agreement: 'Betaling van de raamovereenkomst',
        receipt_financial_compensation: 'Ontvangst van de financiële vergoeding (bruto)',
        definitive_exemption: 'Definitieve vrijstelling',
        end_book_year: 'Einde van het boekjaar',
        start_book_year_correction: 'Begin van het volgend boekjaar: correctie van de overdracht',
        transfer_to_tax_free: 'Overboeking naar belastingvrije reserve tax shelter',
        tax_free_reserve: 'Belastingvrije reserve tax shelter',
        waitaccount_tax_shelter: 'Wachtrekening tax shelter',
        diverse_debt_gallop: 'Diverse schuld aan tax shelter',
        withdrawal_tax_exempt_reserve: 'Onttrekking aan de belastingvrije reserve tax shelter',
        credit_institutions: 'Kredietinstellingen',
        financial_compensation_tax_shelter: 'Financiële vergoeding tax shelter investering',
        estimated_taxes_cost: 'Geraamde belasting tax shelter kost',
        investment: 'Investering in tax shelter',
        estimated_taxes: 'Geraamde belastingen',
        estimated_amount_tax_debts: 'Geraamd bedrag van de belastingschulden',
        to_be_recovered_taxes_advance: 'Terug te vorderen voorheffingen en voorafbetalingen',
        activated_taxes_paid_advances: 'Geactiveerde overschotten van betaalde voorheffingen en belastingen',
        estimated_taxes_liquidation_reserve: 'Geraamde belastingen liquidatiereserve',
        estimated_amount_taxes_liquidation_reserve: 'Geraamd bedrag van de belasting op de liquidatiereserve',
        addition_to_remaining_reserves: 'Toevoeging aan de overige reserves',
        distributable_reserves_liquidation_reserve: 'Beschikbare reserves - liquidatiereserve',
        non_deductible_taxes: 'Niet aftrekbare belastingen',
        effective_tax_shelter_investment: 'Effectieve tax shelter investering',
        maximal_tax_shelter_investment: 'Maximale tax shelter investering',
        remaining_invest_space: 'Resterende investeringsruimte',
        tax_without_tax_shelter_investment: 'Belasting zonder tax shelter investering',
        tax_with_tax_shelter_investment: 'Belasting met tax shelter investering',
        fiscal_profit: 'Fiscaal rendement',
        financial_profit_actual: 'Financieel rendement (netto)',
        total_profit: 'Totaal rendement',
        increased_liquidation_reserve: 'Met een aangelegde liquidatiereserve van',
        warning_overinvestment: 'Opgelet! Het fiscaal voordeel tax shelter wordt gedeeltelijk overgedragen naar het volgende aanslagjaar. Zorg ervoor dat het geraamde overdragen rendement niet verloren gaat!',
        maximal_tax_shelter_pardon: 'Maximale tax shelter vrijstelling',
        carryover_investment: 'Over te dragen investering',
        unavailable: 'niet beschikbaar',
        other_mutations_reserves: 'Andere mutaties reserves',
        credit: 'credit',
        debit: 'debet',
        extra_info: 'Extra Info',
        payment_date: 'Datum van betaling',
        carried_over_investment: 'Overgedragen fiscaal rendement',
        optimal_tax_shelter_investment: 'Optimale tax shelter investering',
        optimal_tax_shelter_pardon: 'Optimale tax shelter vrijstelling'
    },
    localisation: {
        language: 'taal'
    }
};
