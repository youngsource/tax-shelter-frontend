import fr from './fr.js';
import nl from './nl.js';

const translations = {
    nl: nl,
    fr: fr
};

export default translations;
