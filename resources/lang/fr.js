export default {
    tax_shelter: {
        title: 'Tax shelter tool',
        info: `<p><b>Note explicative</b></p>
            <p>Bienvenue au simulateur tax shelter. Ce simulateur peut vous aider à:</p>
            <ul>
                <li>calculez le montant maximal de votre investissement</li>
                <li>déterminez le rendement de votre investissement</li>
                <li>comptabiliser correctement l’investissement tax shelter</li>
            </ul>
            <p>
                Le montant maximal de l’investissement et le rendement sont déterminés sur la base de différentes itérations basées sur la situation comptable et fiscale du l'investisseur.
            </p>
            <p>
               Vous avez l’option d'effectuer une simulation avec un investissement concret ou maximal, et ce, pour une construction maximale de la réserve de liquidation ou d'un montant auto-déclaré.
            </p>
            <aside>
               Attention! Votre rendement tax shelter dépend de nombreux variables. Par exemple, sile taux réduit s'applique, le rendement de l'investissement tax shelter peut être négatif.
            </aside>
            <p>
                L'outil vous donne une première indication, mais il est toujours conseillé de consulter un comptable ou un conseiller fiscal.
            </p>`,
        general: 'Général',
        fiscal_year: 'Exercise d\'imposition',
        max_tax_shelter: 'Investissement tax shelter maximal',
        yes: 'Oui',
        no: 'Non',
        tax_shelter_investment: 'Tax shelter investissement',
        max_liquidation_reserve: 'Réserve de liquidation maximale',
        liquidation_reserve: 'Réserve de liquidation',
        increment_va: 'Majoration VA',
        decreased_tariff: 'Tarif réduit',
        accountancy: 'Comptable',
        profit_before_taxes: 'Bénéfice de l\'exercice avant impôts',
        dividends: 'Dividendes',
        tantiemes: 'Tantièmes',
        other_fiscal_corrections: 'Autres redressements fiscaux',
        other_disposed_expenses_no_deduction: 'Autres dépenses non admises (avec limitation de déduction)',
        other_disposed_expenses_with_deduction: 'Autres dépenses non admises (sans limitation de déduction)',
        deduction_calculations: 'Opérations de soustraction',
        deduction_calculations_without_grain_limitation: 'Déductions sans limitation corbeille',
        deduction_calculations_with_grain_limitation: 'Déductions avec limitation corbeille',
        prepaid_quarter_1: 'Remboursement anticipé 1<sup>er</sup> Trimestre',
        prepaid_quarter_2: 'Remboursement anticipé 2<sup>ème</sup> Trimestriel',
        prepaid_quarter_3: 'Remboursement anticipé 3<sup>ème</sup> Trimestriel',
        prepaid_quarter_4: 'Paiement anticipé 4<sup>ème</sup> Trimestriel',
        reimbursable_withholding_taxes: 'Précomptes remboursables',
        adjustments_previous_years: 'Régularisations, exercises antérieures',
        calculate: 'Calculer',
        calculation_not_representative: 'Attention, les chiffres sont différents de ceux utilisés dans le résultat. Cliquez à nouveau sur le bouton calculer pour rafraîchir le résultat.',
        hide_bookings: 'Masquer les comptabilisations',
        show_bookings: 'Afficher les comptabilisations',
        signature_framework_agreement: 'Signature de conventions-cadre',
        payment_framework_agreement: 'Versement des sommes de la convention-cadre',
        receipt_financial_compensation: 'Perception de l’indemnité rémunérant le préfinancement (brut)',
        definitive_exemption: 'Exonération définitive',
        end_book_year: 'Clôture de l\'exercice',
        start_book_year_correction: 'Début de l\'exercice : correction du report',
        transfer_to_tax_free: 'Dotation aux réserves immunisées tax shelter',
        tax_free_reserve: 'Réserves immunisées tax shelter',
        waitaccount_tax_shelter: 'Compte d’attente',
        diverse_debt_gallop: 'Autres dettes diverses tax shelter',
        withdrawal_tax_exempt_reserve: 'Prélèvements sur les réserves immunisées tax shelter',
        credit_institutions: 'Établissements de crédit: comptes courants',
        financial_compensation_tax_shelter: 'Produits financiers – investissement tax shelter',
        estimated_taxes_cost: 'Charges fiscale estimées Tax Shelter',
        investment: 'Investissement dans le tax shelter',
        estimated_taxes: 'Charges fiscales estimées',
        estimated_amount_tax_debts: 'Montant estimé des dettes fiscales',
        to_be_recovered_taxes_advance: 'Impôts et précomptes à récupérer',
        activated_taxes_paid_advances: 'Excédent de versements d\'impôts et de précomptes portés à l\'actif',
        estimated_taxes_liquidation_reserve: 'Charges fiscales estimées -réserve de liquidation',
        estimated_amount_taxes_liquidation_reserve: 'Dettes fiscales estimées – réserve de liquidation',
        addition_to_remaining_reserves: 'Dotation aux autres réserves',
        distributable_reserves_liquidation_reserve: 'Réserve disponible - réserve de liquidation',
        non_deductible_taxes: 'Impôts non-déductible',
        effective_tax_shelter_investment: 'Investissement dans l\'abri fiscal effectivement',
        maximal_tax_shelter_investment: 'Investissement dans l\'abri fiscal maximal',
        remaining_invest_space: 'Investissement résiduel',
        tax_without_tax_shelter_investment: 'Impôts sans investissement dans l\'abri fiscal',
        tax_with_tax_shelter_investment: 'Impôts avec investissement dans l\'abri fiscal',
        fiscal_profit: 'Rendement fiscal',
        financial_profit_actual: 'Rendement financier (net)',
        total_profit: 'Rendement total',
        increased_liquidation_reserve: 'Avec réserve de liquidation',
        warning_overinvestment: 'Attention! Une partie proportionnelle de l’avantage fiscal du tax shelter est transférée à une exercice ultérieur. Le rendement fiscal peut par conséquent ne pas être indiquées.',
        maximal_tax_shelter_pardon: 'Exonération d\'abri fiscal maximal',
        carryover_investment: 'Investissement a transmettre',
        unavailable: 'indisponible',
        other_mutations_reserves: 'Autres mutations des réserves',
        credit: 'credit',
        debit: 'debet',
        extra_info: 'Extra Info',
        payment_date: 'Date de payement',

        carried_over_investment: 'to be translated - carried over investement'

    },
    localisation: {
        language: 'langue'
    }
};
